import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput, Linking, Alert } from 'react-native';
import WorkItem from './components/workItem';
import { Location, Permissions } from 'expo';

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      workData : [],
      range: "",
      location: null,
    }
  }

  componentDidMount(){
    this.fetchRoadworks(null);
  }

  getLocation = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied'
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
    //alert(location.coords.longitude+", "+location.coords.latitude);
  };

  fetchRoadworks(location){
    let endpoint = 'http://api.gipod.vlaanderen.be/ws/v1/workassignment';
    if(location){
      endpoint +=
        '?point=' +
        location.coords.longitude +
        ',' +
        location.coords.latitude +
        '&radius=' +
        this.state.range;
    } else {
      endpoint += '?city=Gent';
    }
    fetch(endpoint)
      .then(data => data.json())
      .then(jsondata => this.setState({
          workData: jsondata
        }))
      .catch(error => console.error('Error:',error));
  }


  ViewWork = (item) => {
    const coordinates = item.coordinate.coordinates || null;
    if (coordinates && coordinates.length > 1) {
      // Open Google Maps
      Linking.openURL(
        'https://www.google.com/maps/place/' +
          coordinates[1] +
          ',' +
          coordinates[0]
      );
    } else {
      Alert.alert('Coordinates not found','There was an issue with the coordinates.');
    }
  }

  loadItems = async () => {
    this.setState({text: ""});
    await this.getLocation();
    await this.fetchRoadworks(this.state.location);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ScrollView style={styles.scrollContainer} contentContainerStyle={styles.contentContainer}>
          {
            this.state.workData.map((item, index) => {
              return <WorkItem 
                key={item.gipodId} 
                title={item.owner} 
                description={item.description}
                startDate={item.startDateTime}
                endDate={item.endDateTime}
                onClick={() => this.ViewWork(item)} 
              />
            })
          } 
        </ScrollView>
        <View style={styles.bottomButton}>
          <View style={styles.bottomButtonInner}>
            <Text>Binnen de </Text>
            <TextInput 
              style={styles.searchTextInput}
              onChangeText={(text) => this.setState({range: text})}
              value={this.state.range}
              placeholder={"2000"}
            />
            <Text>m</Text>
            <TouchableOpacity style={styles.goButton} onPress={this.loadItems}>
              <Text style={styles.buttonText}> GO </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  scrollContainer: {
    flex: 1,
  },
  contentContainer: {
    backgroundColor: '#fff',
    marginTop: 55,
    paddingBottom: 55,
    marginLeft: 15,
    marginRight: 15,
  },
  bottomButton: {
    backgroundColor: '#F5F3F2',
    height: 50,
    alignItems: 'flex-end',
    lineHeight: 50
  },
  bottomButtonInner: {
    marginRight: 15,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  goButton: {
    marginLeft: 10,
    backgroundColor: '#66D881',
    borderRadius: 5,
    height: 44,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  searchTextInput: {
    margin: 3,
    paddingLeft: 5,
    width: 100,
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'gray',
  },
});
