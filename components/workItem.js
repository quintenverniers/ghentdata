import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';


const WorkItem = (props) => {
    return (
        <TouchableOpacity style={styles.itemContainer} onPress={props.onClick} >
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.subTitle}>{props.description}</Text>
            <Text style={styles.date}>{props.startDate} - {props.endDate}</Text>
        </TouchableOpacity>
    );
}
const styles=StyleSheet.create({
    itemContainer: {
        borderBottomColor: '#dedede',
        borderBottomWidth: 1,
      },
      title: {
        fontWeight: 'bold',
        fontSize: 15,
      },
      subTitle: {
        marginTop: 5,
      },
      date: {
        marginBottom: 5,
        marginTop: 7,
        fontSize: 8,
      }
})

export default WorkItem;
